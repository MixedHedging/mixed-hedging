# Mixed Hedging
This repository will store the system used to manage the mixed hedging and other hedge plants that can be measured and automated.

Automated farming is powerful and effective, and using it for hedge plants exponentially improves yield and productivity, below is the set of mixed hedge plants built with this system...
[learn more](https://www.glebefarmhedging.co.uk/product-category/mixed-hedging-native-hedge-mixes/)

##Todo
- Upload Source
- Comment Code
- Create alternate repositories
- issue tracker setup
- dedicated system page on site
